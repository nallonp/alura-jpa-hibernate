package br.com.alura.loja.util;

import br.com.alura.loja.dao.CategoriaDAO;
import br.com.alura.loja.dao.ClienteDAO;
import br.com.alura.loja.dao.PedidoDAO;
import br.com.alura.loja.dao.ProdutoDAO;
import br.com.alura.loja.modelo.Categoria;
import br.com.alura.loja.modelo.Cliente;
import br.com.alura.loja.modelo.ItemPedido;
import br.com.alura.loja.modelo.Pedido;
import br.com.alura.loja.modelo.Produto;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;

public class BancoUtil {

    private static EntityManager em = JPAUtil.getEntityManager();

    public static void popular() {
        em.getTransaction().begin();
        criarCategoria();
        criarCliente();
        criarProduto();
        criarPedido();
        em.getTransaction().commit();
        em.close();
    }

    private static void criarCategoria() {
        Categoria categoria = new Categoria();
        CategoriaDAO categoriaDAO = new CategoriaDAO(em);
        categoria.setNome("Celulares");
        categoriaDAO.cadastrar(categoria);
    }

    private static void criarCliente() {
        Cliente cliente = new Cliente("Nallon", "1234567");
        ClienteDAO clienteDAO = new ClienteDAO(em);
        clienteDAO.cadastrar(cliente);
    }

    private static void criarProduto() {
        Produto produto = new Produto();
        produto.setNome("Mi6");
        produto.setDescricao("Xiaomi Mi6");
        produto.setPreco(new BigDecimal("1200"));
        produto.setDataCadastro(LocalDate.now());
        CategoriaDAO categoriaDAO = new CategoriaDAO(em);
        produto.setCategoria(categoriaDAO.buscar(1L));
        ProdutoDAO produtoDAO = new ProdutoDAO(em);
        produtoDAO.cadastrar(produto);
    }

    private static void criarPedido() {
        ClienteDAO clienteDAO = new ClienteDAO(em);
        Cliente cliente = clienteDAO.buscar(1L);

        Pedido pedido = new Pedido(cliente);

        ProdutoDAO produtoDAO = new ProdutoDAO(em);
        Produto produto = produtoDAO.buscar(1L);

        ItemPedido itemPedido = new ItemPedido(10, pedido, produto);
        pedido.adicionarItem(itemPedido);

        PedidoDAO pedidoDAO = new PedidoDAO(em);
        pedidoDAO.cadastrar(pedido);
    }
}
