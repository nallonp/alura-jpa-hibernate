package br.com.alura.loja.modelo;

import br.com.alura.loja.dao.ChaveCompostaDAO;
import br.com.alura.loja.util.JPAUtil;

import javax.persistence.EntityManager;

public class CadastrarChaveComposta {
    public static void main(String[] args) {
        EntityManager em = JPAUtil.getEntityManager();
        ChaveCompostaDAO chaveCompostaDAO = new ChaveCompostaDAO(em);
        ChaveComposta chaveComposta = new ChaveComposta(new ChaveCompostaId("Celulares", "Eletronicos"));
        em.getTransaction().begin();
        chaveCompostaDAO.criarChave(chaveComposta);
        em.getTransaction().commit();
    }
}