package br.com.alura.loja.modelo;

import javax.persistence.Embeddable;
import java.io.Serial;
import java.io.Serializable;

@Embeddable
public class ChaveCompostaId implements Serializable {
    @Serial
    private static final long serialVersionUID = -7876052092551820538L;
    private String Nome;
    private String Tipo;

    public ChaveCompostaId() {
    }

    public ChaveCompostaId(String nome, String tipo) {
        Nome = nome;
        Tipo = tipo;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String tipo) {
        Tipo = tipo;
    }

    @Override
    public String toString() {
        return "ChaveCompostaId{" +
                "RowA='" + Nome + '\'' +
                ", RowB='" + Tipo + '\'' +
                '}';
    }
}
