package br.com.alura.loja.modelo;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "chave_composta")
public class ChaveComposta {
    @EmbeddedId
    private ChaveCompostaId chaveCompostaId;

    public ChaveComposta() {
    }

    public ChaveComposta(ChaveCompostaId chaveCompostaId) {
        this.chaveCompostaId = chaveCompostaId;
    }

    public String getNome() {
        return chaveCompostaId.getNome();
    }

    public void setNome(String nome) {
        chaveCompostaId.setNome(nome);
    }

    public String getTipo() {
        return chaveCompostaId.getTipo();
    }

    public void setTipo(String tipo) {
        chaveCompostaId.setNome(tipo);
    }

    @Override
    public String toString() {
        return "ChaveComposta{" +
                "chaveCompostaId=" + chaveCompostaId +
                '}';
    }
}
