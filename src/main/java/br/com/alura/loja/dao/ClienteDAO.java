package br.com.alura.loja.dao;

import br.com.alura.loja.modelo.Cliente;

import javax.persistence.EntityManager;

public class ClienteDAO {
    private final EntityManager entityManager;

    public ClienteDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void cadastrar(Cliente cliente) {
        this.entityManager.persist(cliente);
    }

    public void atualizar(Cliente cliente) {
        this.entityManager.merge(cliente);
    }

    public void remover(Cliente cliente) {
        cliente = this.entityManager.merge(cliente);
        this.entityManager.remove(cliente);
    }

    public Cliente buscar(Long id) {
        return this.entityManager.find(Cliente.class, id);
    }

}
