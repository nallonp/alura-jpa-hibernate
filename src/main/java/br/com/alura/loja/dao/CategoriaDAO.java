package br.com.alura.loja.dao;

import br.com.alura.loja.modelo.Categoria;

import javax.persistence.EntityManager;

public class CategoriaDAO {
    private final EntityManager entityManager;

    public CategoriaDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void cadastrar(Categoria categoria) {
        entityManager.persist(categoria);
    }

    public void atualizar(Categoria categoria) {
        entityManager.merge(categoria);
    }

    public Categoria buscar(Long id) {
        return entityManager.find(Categoria.class, id);
    }
}
