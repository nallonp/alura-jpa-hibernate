package br.com.alura.loja.dao;

import br.com.alura.loja.modelo.ChaveComposta;
import br.com.alura.loja.modelo.ChaveCompostaId;

import javax.persistence.EntityManager;
import java.util.NoSuchElementException;
import java.util.Objects;

public class ChaveCompostaDAO {
    private final EntityManager entityManager;

    public ChaveCompostaDAO(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void criarChave(ChaveComposta chaveComposta) {
        entityManager.persist(chaveComposta);
    }

    public ChaveComposta buscarChave(ChaveCompostaId chaveCompostaId) {
        ChaveComposta chaveComposta = entityManager.find(ChaveComposta.class, chaveCompostaId);
        if (Objects.isNull(chaveComposta))
            throw new NoSuchElementException("Elemento não encontrado!");
        return chaveComposta;
    }
}