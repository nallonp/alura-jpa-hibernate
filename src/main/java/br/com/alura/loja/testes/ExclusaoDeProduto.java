package br.com.alura.loja.testes;

import br.com.alura.loja.dao.ProdutoDAO;
import br.com.alura.loja.modelo.Produto;
import br.com.alura.loja.util.JPAUtil;

import javax.persistence.EntityManager;

public class ExclusaoDeProduto {
    public static void main(String[] args) {
        Produto produto = new Produto();
        produto.setId(1L);

        EntityManager em = JPAUtil.getEntityManager();
        ProdutoDAO produtoDAO = new ProdutoDAO(em);
        em.getTransaction().begin();
        produtoDAO.remover(produto);
        em.getTransaction().commit();
        em.close();
    }
}
