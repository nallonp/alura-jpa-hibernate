package br.com.alura.loja.testes;

import br.com.alura.loja.dao.PedidoDAO;
import br.com.alura.loja.modelo.Pedido;
import br.com.alura.loja.util.BancoUtil;
import br.com.alura.loja.util.JPAUtil;

import javax.persistence.EntityManager;

public class PerformanceDeConsultas {
    public static void main(String[] args) {
        BancoUtil.popular();
        EntityManager em = JPAUtil.getEntityManager();
        PedidoDAO pedidoDAO = new PedidoDAO(em);
        Pedido pedido = pedidoDAO.buscarPedidoComCliente(1L);
        //Pedido pedido = em.find(Pedido.class, 1L);
        em.close();
        System.out.println("--------------------");
        System.out.println(pedido.getCliente().getDadosPessoais().getNome()); //LazyInitializationException
    }
}
