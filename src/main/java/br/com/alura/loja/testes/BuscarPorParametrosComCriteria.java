package br.com.alura.loja.testes;

import br.com.alura.loja.dao.ProdutoDAO;
import br.com.alura.loja.modelo.Produto;
import br.com.alura.loja.util.JPAUtil;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

public class BuscarPorParametrosComCriteria {
    public static void main(String[] args) {
        /* BancoUtil.popular();*/
        EntityManager em = JPAUtil.getEntityManager();
        ProdutoDAO produtoDAO = new ProdutoDAO(em);
        List<Produto> produtos = produtoDAO.buscarPorParametrosComCriteria("A72", new BigDecimal(3000), null);
        produtos.forEach(System.out::println);
    }
}
