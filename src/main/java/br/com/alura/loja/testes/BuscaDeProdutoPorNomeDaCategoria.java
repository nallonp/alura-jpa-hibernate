package br.com.alura.loja.testes;

import br.com.alura.loja.dao.ProdutoDAO;
import br.com.alura.loja.modelo.Produto;
import br.com.alura.loja.util.BancoUtil;
import br.com.alura.loja.util.JPAUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class BuscaDeProdutoPorNomeDaCategoria {
    public static void main(String[] args) {
        BancoUtil.popular();
        EntityManager em = JPAUtil.getEntityManager();
        ProdutoDAO produtoDAO = new ProdutoDAO(em);
        List<Produto> produtos = produtoDAO.buscarPorCategoria("Celulares");
        produtos.forEach(p -> System.out.println(p.toString()));
    }
}
