package br.com.alura.loja.testes;

import br.com.alura.loja.dao.CategoriaDAO;
import br.com.alura.loja.dao.ProdutoDAO;
import br.com.alura.loja.modelo.Categoria;
import br.com.alura.loja.modelo.Produto;
import br.com.alura.loja.util.JPAUtil;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;

public class CadastroDeProduto {
    public static void main(String[] args) {
        Categoria categoria = new Categoria();
        Produto produto = new Produto();
        categoria.setNome("Celulares");
        produto.setNome("Mi6");
        produto.setDescricao("Xiaomi Mi6");
        produto.setPreco(new BigDecimal("1200"));
        produto.setDataCadastro(LocalDate.now());
        produto.setCategoria(categoria);
        EntityManager em = JPAUtil.getEntityManager();
        ProdutoDAO produtoDAO = new ProdutoDAO(em);
        CategoriaDAO categoriaDAO = new CategoriaDAO(em);
        em.getTransaction().begin();
        categoriaDAO.cadastrar(categoria);
        produtoDAO.cadastrar(produto);
        em.getTransaction().commit();
        em.close();
    }
}
