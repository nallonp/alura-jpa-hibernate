package br.com.alura.loja.testes;

import br.com.alura.loja.dao.ProdutoDAO;
import br.com.alura.loja.modelo.Produto;
import br.com.alura.loja.util.JPAUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class BuscaDeProdutoPorID {
    public static void main(String[] args) {
        EntityManager em = JPAUtil.getEntityManager();
        ProdutoDAO produtoDAO = new ProdutoDAO(em);
        Produto produto = produtoDAO.buscar(1L);
        System.out.println(produto);
        List<Produto> produtos = produtoDAO.buscarTodos();
        produtos.forEach(p -> System.out.println(p.toString()));
    }
}
