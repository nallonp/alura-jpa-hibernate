package br.com.alura.loja.testes;

import br.com.alura.loja.dao.PedidoDAO;
import br.com.alura.loja.util.BancoUtil;
import br.com.alura.loja.util.JPAUtil;

import javax.persistence.EntityManager;

public class FuncaoDeAgregacao {
    public static void main(String[] args) {
        BancoUtil.popular();
        EntityManager em = JPAUtil.getEntityManager();
        PedidoDAO pedidoDAO = new PedidoDAO(em);
        System.out.println("Valor total vendido: " + pedidoDAO.valorTotalVendido());
    }
}
