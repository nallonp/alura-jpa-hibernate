package br.com.alura.loja.testes;

import br.com.alura.loja.dao.ProdutoDAO;
import br.com.alura.loja.modelo.Produto;
import br.com.alura.loja.util.JPAUtil;

import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class AlteracaoDeProduto {
    public static void main(String[] args) {
        Produto produto = new Produto();
        produto.setId(1L);
        produto.setNome("Mi6");
        produto.setPreco(new BigDecimal("1100"));

        EntityManager em = JPAUtil.getEntityManager();
        ProdutoDAO produtoDAO = new ProdutoDAO(em);
        em.getTransaction().begin();
        produtoDAO.atualizar(produto);
        em.getTransaction().commit();
        em.close();
    }
}
