package br.com.alura.loja.testes;

import br.com.alura.loja.dao.CategoriaDAO;
import br.com.alura.loja.modelo.Categoria;
import br.com.alura.loja.util.JPAUtil;

import javax.persistence.EntityManager;

public class AlteracaoDeCategoria {
    public static void main(String[] args) {
        Categoria categoria = new Categoria();
        categoria.setId(1L);
        categoria.setNome("Smartphones");
        EntityManager em = JPAUtil.getEntityManager();
        CategoriaDAO categoriaDAO = new CategoriaDAO(em);
        em.getTransaction().begin();
        categoriaDAO.atualizar(categoria);
        em.flush();
        em.getTransaction().rollback();
        em.close();
    }
}
